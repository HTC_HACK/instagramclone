<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="static uz.pdp.validation.ValidationResult.SUCCESSFULLY_LOGOUT" %><%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/5/22
  Time: 6:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logout</title>
    <%@ include file="/layouts/link.html" %>
</head>
<body>
<%

    session.invalidate();
    request.setAttribute("logout",SUCCESSFULLY_LOGOUT.name());
%>
<div class="container" style="padding-top: 3%">

    <c:if test="${logout!=null}">
        <div class="alert alert-info alert-dismissible fade show" role="alert">${logout}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        </div>
    </c:if>
    <a href="/login">Login</a>
</div>

<%@ include file="/layouts/scripts.html" %>
</body>
</html>
