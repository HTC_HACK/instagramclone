<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logout</title>
    <%@ include file="/layouts/link.html" %>
</head>
<body>
<div class="container" style="padding-top: 3%">

    <c:if test="${successregister!=null}">
        <div class="alert alert-info alert-dismissible fade show" role="alert">${successregister}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        </div>
    </c:if>
    <a href="/login">Login</a>
</div>

<%@ include file="/layouts/scripts.html" %>
</body>
</html>
