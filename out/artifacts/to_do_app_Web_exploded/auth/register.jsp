<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <%@ include file="/layouts/link.html" %>
    <style>
        .divider:after,
        .divider:before {
            content: "";
            flex: 1;
            height: 1px;
            background: #eee;
        }
    </style>
</head>
<body>
<section class="vh-100">

    <div class="container py-5 h-100">
        <div class="row d-flex align-items-center justify-content-center h-100">
            <div class="col-md-7 col-lg-5 col-xl-5">
                <c:if test="${result!=null}">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">${result}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                </c:if>
                <c:if test="${match_error!=null}">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">${match_error}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                </c:if>
                <c:if test="${error!=null}">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">${error}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                </c:if>
                <form action="register" method="post">
                    <!-- Email input -->
                    <div class="form-outline mb-4">
                        <input type="text" name="email" id="form1Example13" class="form-control form-control-lg" required/>
                        <label class="form-label" for="form1Example13">Email address</label>
                    </div>

                    <!-- Password input -->
                    <div class="form-outline mb-4">
                        <input type="password" name="password" id="form1Example23"
                               class="form-control form-control-lg" required/>
                        <label class="form-label" for="form1Example23">Password</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input type="password" name="confirm_password" id="form2Example23"
                               class="form-control form-control-lg" required/>
                        <label class="form-label" for="form2Example23">Confirm Password</label>
                    </div>


                    <!-- Submit button -->
                    <button type="submit" class="btn btn-info btn-lg btn-block"> Register</button>

                    <div class="divider d-flex align-items-center my-4">
                        <p class="text-center fw-bold mx-3 mb-0 text-muted">OR</p>
                    </div>

                    <a class="btn btn-dark btn-lg btn-block" href="/login"> Login
                    </a>

                </form>
            </div>
        </div>
    </div>
</section>

<%@ include file="/layouts/scripts.html" %>
</body>
</html>
