<%@ page import="static uz.pdp.validation.ValidationResult.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Post</title>
    <%@ include file="/layouts/link.html" %>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="/layouts/menu.jsp" %>

<div class="container" style="padding: 3% 10%;">
    <a href="/profile" class="btn btn-warning"><i class="fas fa-arrow-left"></i> Back</a>
    <div class="row">
        <div class="col-md-9" style="padding-top:2rem">
            <form action="createPost" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="hidden" name="user_id" value="${currentUser.id}">
                    <label>Title</label>
                    <input type="text" name="title" class="form-control"  required>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea type="text" name="description" cols="5" rows="5" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <label>Upload File (max size 2MB)</label>
                    <input style="background: #efefef;border:none" type="file" name="file" class="form-control" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info"><i class="fas fa-plus"> Create</i> </button>
                </div>
            </form>
        </div>
    </div>
</div>

<%@ include file="/layouts/scripts.html" %>
</body>
</html>
