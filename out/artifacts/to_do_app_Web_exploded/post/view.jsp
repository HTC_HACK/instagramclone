<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
<head>
    <title>Detail</title>
    <%@ include file="/layouts/link.html" %>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="/layouts/menu.jsp" %>
<div class="container" style="padding: 3% 10%;">
    <div class="row">
        <div class="col-md-6">
            <div class="card" style="width: 100%;margin-top: 2%">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><a style="text-decoration: none;color: #000"
                                                   href="/showProfile?user_id=${post.userid}"> <c:if
                            test="${post.profile_img!=null}">
                        <img
                                src="data:image/png;base64, ${post.profile_img}"
                                width="30px" height="30px" style="border-radius: 50%">
                    </c:if> ${post.firstName} ${post.lastName}</a>
                    </li>
                </ul>
                <img class="card-img-top"
                     src="data:image/png;base64,${post.post_path}"
                     alt="Card image cap" style="height: 330px">
                <div class="card-body">

                    <a style="text-decoration: none;color: black"
                       href="/detailPost?post_id=${post.id}"> ${post.commentCount} <i
                            class="fas fa-comment"></i></a>
                    <a style="text-decoration: none;color: black"
                       href="/like?post_id=${post.id}"> ${post.likesCount} <i class="fa fa-thumbs-up icon"></i></a>
                    <c:if test="${post.saved>0}">
                        <a style="text-decoration: none;color: black;float: right" href="/saved?post_id=${post.id}">Saved
                            <i class="fas fa-bookmark"></i> </a>
                    </c:if>
                    <c:if test="${post.saved<1}">
                        <a style="text-decoration: none;color: black;float: right" href="/saved?post_id=${post.id}">UnSaved
                            <i class="fas fa-bookmark"></i> </a>
                    </c:if>

                    <p class="card-text">${post.title} ${post.description}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">

            <ul class="list-group" style="height: 70vh;overflow-y: scroll">
                <li class="list-group-item disabled" aria-disabled="true">Commnets</li>
                <c:forEach items="${post.comment}" var="comment">
                    <li class="list-group-item"><a style="text-decoration: none;"
                                                   href="/showProfile?user_id=${comment.userid}"> ${comment.firstname} ${comment.lastname}</a> : ${comment.description}</li>
                </c:forEach>
            </ul>
            <form method="post" action="/comment">
                <input name="post_id" type="hidden" value="${post.id}">
                <input type="hidden" name="redirect_url" value="/detailPost?post_id=${post.id}">
                <input name="description" class="form-control" type="search" placeholder="Add comment"
                       style="width: 85%">
                <div style="float: right;margin-top: -2.3rem">
                    <button class="btn btn-outline-info my-2 my-sm-0" type="submit"><i
                            class="fas fa-paper-plane send"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<%@ include file="/layouts/scripts.html" %>
</body>
</html>
