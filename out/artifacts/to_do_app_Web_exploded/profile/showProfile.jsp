<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
<head>
    <title>User </title>
    <%@ include file="/layouts/link.html" %>
    <style>

        .active-tabs {
            background: #efefef;
        }

        .active-tabs input {
            opacity: 0;
            display: none;
            visibility: hidden;
        }


        .btn:hover {
            opacity: 1;
        }

        .active-tabs input:checked + label {
            background: #fff;
            opacity: 1;
            color: #1babbb;
        }

        .tabs-container {
            background: #efefef;
        }

        .tab-1,
        .tab-2,
        .tab-3,
        .tab-4,
        .tab-5,
        .tab-6,
        .tab-7 {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            visibility: hidden;
        }

        .tab-2,
        .tab-4,
        .tab-6 {
            height: 300px;
        }

        .tab-1 p,
        .tab-2 p,
        .tab-3 p,
        .tab-4 p,
        .tab-5 p,
        .tab-6 p,
        .tab-7 p {
            color: #1babbb;
            font-family: "Open Sans";
            font-size: 50px;
            line-height: 200px;
            text-align: center;
        }


        .btn-1:checked ~ .tabs-container .tab-1,
        .btn-2:checked ~ .tabs-container .tab-2,
        .btn-3:checked ~ .tabs-container .tab-3,
        .btn-4:checked ~ .tabs-container .tab-4 {
            position: relative;
            visibility: visible;
            top: 0;
            left: 0;
            opacity: 1;
        }

        .column {
            position: relative;
        }

        .column .text {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 10;
            opacity: 0;
            transition: all 0.8s ease;
        }

        .column .text h1 {
            margin: 0;
            color: white;
        }

        .column:hover .text {
            opacity: 1;

        }

        .column:hover img {
            -webkit-filter: grayscale(100%) blur(3px);
            filter: grayscale(100%) blur(3px);
        }


    </style>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="/layouts/menu.jsp" %>
<div class="container" style="padding: 3% 10%;">
    <div class="row">
        <div class="col-md-3">
            <div class="profile-img" style="text-align: center;">
                <c:if test="${currentUser.image_path!=null}">
                    <img src="data:image/png;base64, ${currentUser.image_path}" width="140px" height="140px"
                         style="border-radius: 50%"
                         alt=""/>
                </c:if>
            </div>
        </div>
        <div class="col-md-9">
            <div class="about" style="text-align: left">
                <h3>${currentUser.username}
                    <c:if test="${num>0}">
                        <a href="sendMessage?message_id=${currentUser.id}" class="btn btn-info">Message</a>
                        <a href="/unfollowRequest?follow_id=${currentUser.id}" class="btn btn-danger">UnFollow</a>
                    </c:if>
                    <c:if test="${num<1}">
                        <a href="/followRequest?follow_id=${currentUser.id}" class="btn btn-info">Follow</a>
                    </c:if>
                </h3>
                <br>
                <h6>
                    ${currentUser.firstName}<br>${currentUser.lastName}<br>${currentUser.email}
                </h6>
                <br>
                <h4>
                    <button class="btn btn-default"> ${count} Posts</button>
                    <a href="/followers?user_id=${currentUser.id}" class="btn btn-default"> ${followers} Followers</a>
                    <a href="/following?user_id=${currentUser.id}" class="btn btn-default"> ${following} Following</a>
                </h4>

            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12" style="text-align: center">
            <h4>
                <div class="active-tabs">
                    <input type="radio" name="active_tabs" id="btn-1" class="btn-1" checked>
                    <label for="btn-1" class="btn">Posts</label>

                    <input type="radio" name="active_tabs" id="btn-2" class="btn-2">
                    <label for="btn-2" class="btn">Videos</label>

                    <input type="radio" name="active_tabs" id="btn-3" class="btn-3">
                    <label for="btn-3" class="btn">Saved</label>

                    <input type="radio" name="active_tabs" id="btn-4" class="btn-4">
                    <label for="btn-4" class="btn">Tagged</label>

                    <hr>

                    <div class="tabs-container">

                        <div class="tab-1">
                            <div class="row">
                                <c:forEach items="${postList}" var="post">
                                    <c:if test="${post.file_name!=null}">
                                        <!-- Button trigger modal -->
                                        <div class="col-md-4" style="text-align: center;padding-top: 1rem">
                                            <a href="" class="column col-xs-6" data-toggle="modal"
                                               data-target="#${post.title}${post.description}"><span class="text"><h3
                                                    style="color: #000;font-weight: bold">${post.title}</h3>
                                                                                    </span><img
                                                    class="card-img-top"
                                                    src="data:image/png;base64, ${post.file_name}"
                                                    alt="${post.title}"
                                                    width="200px" height="180px"></a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="${post.title}${post.description}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title"
                                                                id="exampleModalLongTitle">${post.title}
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img
                                                                    class="card-img-top"
                                                                    src="data:image/png;base64, ${post.file_name}"
                                                                    alt="${post.title}"
                                                                    width="200px" height="180px">

                                                        </div>
                                                        <div class="modal-footer">
                                                            <a class="btn btn-info" href="/detailPost?post_id=${post.id}">Detail</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>

                        <div class="tab-2">
                            <p>Videos</p>
                        </div>

                        <div class="tab-3">
                            <p>Saved</p>
                        </div>

                        <div class="tab-4">
                            <p>Tagged</p>
                        </div>

                    </div>

                </div>
            </h4>
        </div>
    </div>
</div>
<%@ include file="/layouts/scripts.html" %>
</body>
</html>
