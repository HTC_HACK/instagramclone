<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<html>
<head>
    <title>Followers</title>
    <%@ include file="/layouts/link.html" %>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="/layouts/menu.jsp" %>
<div class="container" style="padding: 3% 7%;">

    <div class="row">
        <div class="col-md-8 offset-2">
            <div class="card" style="width: 100%">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Followers</li>
                    <c:forEach items="${allFollowers}" var="user">
                        <li class="list-group-item">
                            <a href="/showProfile?user_id=${user.id}" style="text-decoration: none;">
                            <c:if test="${user.image_path!=null}">
                                <img
                                        src="data:image/png;base64, ${user.image_path}"
                                        width="30px" height="30px" style="border-radius: 50%">
                            </c:if>
                                    ${user.firstName} ${user.lastName}</a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>

</div>
<%@ include file="/layouts/scripts.html" %>
</body>
</html>

