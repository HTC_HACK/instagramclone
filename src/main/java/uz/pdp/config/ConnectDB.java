package uz.pdp.config;



//Asadbek Xalimjonov 2/4/22 11:25 AM

import java.sql.Connection;
import java.sql.DriverManager;

import static uz.pdp.config.Constants.*;

public class ConnectDB {

    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(dbUrl, username, password);
        } catch (Exception e) {
            System.out.println(e);
        }
        return con;
    }

}
