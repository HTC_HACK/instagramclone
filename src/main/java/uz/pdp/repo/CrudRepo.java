package uz.pdp.repo;

import java.util.List;



//Asadbek Xalimjonov 2/4/22 9:42 PM

public interface CrudRepo<T,P> {

    List<T> getAll();

    boolean save(T t);

    boolean edit(T t, P id);

    T findById(P id);

    T findByData(T t);

    boolean delete(P id);

}