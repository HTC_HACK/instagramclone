package uz.pdp.dto;


//Asadbek Xalimjonov 2/12/22 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class PostDto {

    private Integer id;
    private String title;
    private String description;
    private String post_path;
    private Integer userid;
    private String firstName;
    private String lastName;
    private String profile_img;
    private Integer commentCount;
    private Integer saved;
    private Integer likesCount;
    private List<CommentDto> comment = new ArrayList<>();
}
