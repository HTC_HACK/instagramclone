package uz.pdp.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/13/22 4:12 PM

public class CommentDto {

    private Integer commentid;
    private Integer postid;
    private String description;
    private Integer userid;
    private String firstname;
    private String lastname;
}
