package uz.pdp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data


//Asadbek Xalimjonov 2/14/22 10:23 AM

public class UserDto {

    private Integer id;
    private String firstName;
    private String lastName;
    private Integer amount;
    private String image_path;

}
