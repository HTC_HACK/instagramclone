package uz.pdp.dto;


//Asadbek Xalimjonov 2/14/22 11:03 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.model.message.Message;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data


public class MessageDto {


    private Integer id;
    private String firstName;
    private String lastName;
    private String image_path;
    private List<Message> messageList = new ArrayList<>();

}
