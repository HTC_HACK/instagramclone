package uz.pdp.model.notification;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/8/22 5:19 PM

public class Follow {

    private Integer id;
    private Integer sender_id;
    private Integer receiver_id;
    private boolean status;

}
