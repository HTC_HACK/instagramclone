package uz.pdp.model.message;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/14/22 9:17 AM

public class Message {
    private Integer id;
    private Integer sender_id;
    private Integer receiver_id;
    private String description;
    private boolean status;
    private LocalDateTime created_at;
}
