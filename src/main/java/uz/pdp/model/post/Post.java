package uz.pdp.model.post;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/7/22 4:57 PM

public class Post {
    private Integer id;
    private String title;
    private String description;
    private String file_name;
    private String file_path;
    private Integer user_id;
    private boolean isVideo;
    private LocalDateTime createdAt = LocalDateTime.now();
    private LocalDateTime updatedAt = LocalDateTime.now();
}
