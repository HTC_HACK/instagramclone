package uz.pdp.model.post;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/7/22 5:06 PM

public class Saved {

    private Integer id;
    private Integer user_id;
    private Integer post_id;
    private LocalDateTime createdAt = LocalDateTime.now();
    private LocalDateTime updatedAt = LocalDateTime.now();

}
