package uz.pdp.model.post;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/7/22 5:01 PM

public class Dislike {
    private Integer id;
    private Integer post_id;
    private Integer user_id;
}
