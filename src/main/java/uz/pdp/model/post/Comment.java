package uz.pdp.model.post;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 2/7/22 5:04 PM

public class Comment {
    private Integer id;
    private String description;
    private Integer user_id;
    private Integer post_id;
    private Integer comment_id;
    private LocalDateTime createdAt = LocalDateTime.now();
    private LocalDateTime updatedAt = LocalDateTime.now();
}
