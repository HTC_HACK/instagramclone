package uz.pdp.service;


//Asadbek Xalimjonov 2/12/22 10:57 PM

import uz.pdp.config.ConnectDB;
import uz.pdp.model.post.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class CommentService {


    public boolean saveComment(Comment comment) {


        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into comments(description, post_id, user_id) values (?,?,?)");
            ps.setString(1, comment.getDescription());
            ps.setInt(2, comment.getPost_id());
            ps.setInt(3, comment.getUser_id());
            status = ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;


    }

}
