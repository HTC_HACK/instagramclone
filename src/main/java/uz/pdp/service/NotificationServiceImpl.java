package uz.pdp.service;


//Asadbek Xalimjonov 2/9/22 4:12 PM

import uz.pdp.config.ConnectDB;
import uz.pdp.model.User;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NotificationServiceImpl {


    public boolean makeFollow(Integer sender_id, Integer receiver_id) {
        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into followers(sender_id, receiver_id) values (?,?)");
            ps.setInt(1, sender_id);
            ps.setInt(2, receiver_id);
            status = ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;
    }


    public List<User> getNotification(String email) {


        List<User> userList = new ArrayList<>();

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select p.id,p.email,p.firstname,p.lastname,p.username,p.image_path,log_notification.is_read as new\n" + "from log_notification\n" + "join users u\n" + "on u.id=log_notification.receiver_id\n" + "join users p\n" + "     on p.id=log_notification.sender_id\n" + "where u.email='" + email + "';");

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                ser.setUsername(resultSet.getString("username"));
                ser.setPrivateAccount(resultSet.getBoolean("new"));

                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                userList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;

    }

    public int following(Integer user_id) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select count(receiver_id)\n" + "from users\n" + "join followers f on users.id = f.receiver_id\n" + "where sender_id=" + user_id + ";");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception ignored) {
        }
        return 0;
    }

    public int followers(Integer user_id) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select count(sender_id)\n" + "from users\n" + "         join followers f on users.id = f.sender_id\n" + "where receiver_id=" + user_id + ";");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception ignored) {
        }
        return 0;
    }

    public int followersFind(Integer receiver_id, Integer sender_id) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select count(f.receiver_id)\n" + "from users\n" + "         join followers f on users.id = f.sender_id\n" + "where receiver_id=" + receiver_id + " and sender_id=" + sender_id + " ;");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception ignored) {
        }
        return 0;
    }

    public void makeUnFollow(Integer sender_id, Integer receiver_id) {
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("delete from followers where sender_id=? and receiver_id=?;");
            ps.setInt(1, sender_id);
            ps.setInt(2, receiver_id);
            ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void clearAll(Integer authUser) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;


        try {
            preparedStatement = con.prepareStatement("update log_notification\n" + "set is_read=true\n" + "where receiver_id=" + authUser + " and is_read=false;");
            preparedStatement.executeUpdate();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
