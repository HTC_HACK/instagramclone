package uz.pdp.service;


//Asadbek Xalimjonov 2/14/22 9:42 AM

import uz.pdp.config.ConnectDB;
import uz.pdp.dto.UserDto;
import uz.pdp.model.message.Message;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageService {


    public List<UserDto> getAllFriends(Integer user_id) {
        String query = "select distinct p.*, COALESCE(a.amount, 0) as amount\n" +
                "from (select users.id, users.firstname, users.lastname, users.image_path\n" +
                "      from users\n" +
                "               join followers f on users.id = f.sender_id\n" +
                "      where receiver_id = " +
                "" + user_id + "\n" +
                "      union all\n" +
                "      select users.id, users.firstname, users.lastname, users.image_path\n" +
                "      from users\n" +
                "               join followers f on users.id = f.receiver_id\n" +
                "      where sender_id = " + user_id + ") p\n" +
                "         LEFT JOIN\n" +
                "     (select sender_id, count(status) as amount\n" +
                "      from messages\n" +
                "      where status = false and receiver_id="+user_id+"\n" +
                "      group by sender_id) a\n" +
                "     ON p.id = a.sender_id\n" +
                "ORDER BY amount desc;";
        List<UserDto> userList = new ArrayList<>();

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserDto ser = new UserDto();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                ser.setAmount(resultSet.getInt("amount"));

                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                userList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;
    }

    public List<Message> getChatRoomMessage(Integer you, int friend) {


        String query = "SELECT u.id as id,u.receiver_id as receiver,u.sender_id as sender,u.description as description,u.created_at as date\n" + "FROM messages u\n" + "where u.sender_id = " + you + " and u.receiver_id=" + friend + "\n" + "   or u.receiver_id = " + you + " and u.sender_id=" + friend + "\n" + "order by u.created_at DESC";
        List<Message> messageList = new ArrayList<>();

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Message ser = new Message();
                ser.setDescription(resultSet.getString("description"));
                ser.setId(resultSet.getInt("id"));
                ser.setReceiver_id(resultSet.getInt("receiver"));
                ser.setSender_id(resultSet.getInt("sender"));
                ser.setCreated_at(resultSet.getTimestamp("date").toLocalDateTime());
                messageList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return messageList;
    }

    public boolean saveMessage(Integer sender, int receiver, String description) {

        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into messages(sender_id, receiver_id, description) values (?,?,?)");
            ps.setInt(1, sender);
            ps.setInt(2, receiver);
            ps.setString(3, description);
            status = ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;
    }

    public void readAll(int friend, Integer you) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("update messages\n" + "set status=true\n" + "where receiver_id=" + you + " and sender_id=" + friend + " and status=false;");
            preparedStatement.executeUpdate();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
