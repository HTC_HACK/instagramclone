package uz.pdp.service;


//Asadbek Xalimjonov 2/13/22 2:44 PM

import uz.pdp.config.ConnectDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SavedService {

    public boolean savePost(int user_id, Integer post_id) {

        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();

            PreparedStatement preparedStatement = con.prepareStatement("select count(*)\n" + "from saved_post\n" + "where post_id=" + post_id + " and user_id=" + user_id + " ;");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int anInt = resultSet.getInt(1);
                if (anInt > 0) {
                    PreparedStatement prep = con.prepareStatement("delete from saved_post where post_id=" + post_id + " and user_id=" + user_id + " ;");
                    prep.executeUpdate();
                } else {
                    PreparedStatement ps = con.prepareStatement("insert into saved_post(post_id, user_id) values (?,?)");
                    ps.setInt(1, post_id);
                    ps.setInt(2, user_id);
                    status = ps.executeUpdate();
                }
            }


            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;

    }
}

