package uz.pdp.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import uz.pdp.config.ConnectDB;
import uz.pdp.dto.CommentDto;
import uz.pdp.dto.PostDto;
import uz.pdp.model.post.Post;
import uz.pdp.repo.CrudRepo;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


//Asadbek Xalimjonov 2/7/22 6:32 PM

public class PostServiceImpl implements CrudRepo<Post, Integer> {
    @Override
    public List<Post> getAll() {


        return null;
    }

    public List<PostDto> getSavedPost(Integer user_id) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            List<PostDto> arrayList = new ArrayList<>();
            preparedStatement = con.prepareStatement("select posts.id as postid,\n" + "       posts.title as title,\n" + "       posts.description as description,\n" + "       posts.file_path as post_path,\n" + "       u.id as userid,u.firstname as firstname,\n" + "       u.lastname as lastname,\n" + "       u.image_path as profile_img\n" + "from posts\n" + "join saved_post sp on posts.id = sp.post_id\n" + "join users u on u.id = posts.user_id\n" + "where sp.user_id=" + user_id + "\n" + "order by posts.\"createdAt\"");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                PostDto post = new PostDto();
                post.setId(resultSet.getInt("postid"));
                post.setTitle(resultSet.getString("title"));
                post.setDescription(resultSet.getString("description"));
                post.setUserid(resultSet.getInt("userid"));
                post.setFirstName(resultSet.getString("firstname"));
                post.setLastName(resultSet.getString("lastname"));
                post.setCommentCount(countComments(resultSet.getInt("postid")));
                post.setLikesCount(countLikes(resultSet.getInt("postid")));
                post.setSaved(bookMark(resultSet.getInt("postid"), user_id));


                try {
                    String imgName = "" + resultSet.getString("post_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setPost_path(b64);
                } catch (Exception e) {

                }
                try {
                    String imgName = "" + resultSet.getString("profile_img");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setProfile_img(b64);
                } catch (Exception e) {
                }
                arrayList.add(post);
            }
            con.close();
            return arrayList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;


    }

    public List<Post> getAllById(Integer authUser) {

        List<Post> postList = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select * from posts where user_id=" + authUser);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Post post = new Post();
                post.setId(resultSet.getInt("id"));
                post.setTitle(resultSet.getString("title"));
                post.setDescription(resultSet.getString("description"));
                post.setUser_id(resultSet.getInt("user_id"));
                try {
                    String imgName = "" + resultSet.getString("file_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    if (b64.length() > 0) {
                        post.setFile_name(b64);
                    }
                } catch (Exception e) {
                }
                postList.add(post);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return postList;

    }

    @Override
    public boolean save(Post post) {
        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into posts(title, description, file_name, file_path, user_id, isvideo) values (?,?,?,?,?,?)");
            ps.setString(1, post.getTitle());
            ps.setString(2, post.getDescription());
            ps.setString(3, post.getFile_name());
            ps.setString(4, post.getFile_path());
            ps.setInt(5, post.getUser_id());
            ps.setBoolean(6, post.isVideo());
            status = ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;
    }

    @Override
    public boolean edit(Post post, Integer id) {
        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("update posts set title=?,description=?,file_name=?,file_path=? where posts.id=?");
            ps.setString(1, post.getTitle());
            ps.setString(2, post.getDescription());
            ps.setString(3, post.getFile_name());
            ps.setString(4, post.getFile_path());
            ps.setInt(5, id);
            status = ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;
    }

    public boolean editDes(Post post, Integer id) {
        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("update posts set title=?,description=? where posts.id=?");
            ps.setString(1, post.getTitle());
            ps.setString(2, post.getDescription());
            ps.setInt(3, id);
            status = ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;
    }

    @Override
    public Post findById(Integer id) {
        return null;
    }

    @Override
    public Post findByData(Post post) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }


    public boolean deletePost(Integer id, Integer user_id) {
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("delete from posts where id=? and user_id=?");
            ps.setInt(1, id);
            ps.setInt(2, user_id);
            ps.executeUpdate();
            con.close();
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public PostDto findByIdD(int post_id, int user_id) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;


        try {
            PostDto post = new PostDto();
            preparedStatement = con.prepareStatement("select posts.id as postid,\n" +
                    "       posts.title as title,\n" +
                    "       posts.description as description,\n" +
                    "       posts.file_path as post_path,\n" +
                    "       u.id as userid,\n" +
                    "       u.firstname as firstName,\n" +
                    "       u.lastname as lastName,\n" +
                    "       u.image_path as profile_path\n" +
                    "from posts\n" +
                    "join users u on u.id = posts.user_id\n" +
                    "where posts.user_id="+user_id+" and posts.id="+post_id+"\n");

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                post.setId(resultSet.getInt("postid"));
                post.setTitle(resultSet.getString("title"));
                post.setDescription(resultSet.getString("description"));
                post.setUserid(resultSet.getInt("userid"));
                post.setFirstName(resultSet.getString("firstName"));
                post.setLastName(resultSet.getString("lastName"));
                post.setCommentCount(countComments(resultSet.getInt("postid")));
                post.setLikesCount(countLikes(resultSet.getInt("postid")));
                post.setSaved(bookMark(resultSet.getInt("postid"), user_id));


                try {
                    String imgName = "" + resultSet.getString("post_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setPost_path(b64);
                } catch (Exception e) {

                }
                try {
                    String imgName = "" + resultSet.getString("profile_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setProfile_img(b64);
                } catch (Exception e) {
                }
            }
            con.close();
            return post;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public Post findById(int post_id, Integer id) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            Post post = new Post();
            preparedStatement = con.prepareStatement("select * from posts where user_id=" + id + " and id=" + post_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                post.setId(resultSet.getInt("id"));
                post.setTitle(resultSet.getString("title"));
                post.setDescription(resultSet.getString("description"));
                post.setUser_id(resultSet.getInt("user_id"));
                try {
                    String imgName = "" + resultSet.getString("file_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setFile_name(b64);
                } catch (Exception e) {
                }
            }
            con.close();
            return post;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }


    public List<PostDto> getHomePost(Integer user_id) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            List<PostDto> arrayList = new ArrayList<>();
            preparedStatement = con.prepareStatement("select *\n" + "from (select p.id             as id,p.title as title,\n" + "             p.description    as description,\n" + "             p.file_path      as post_path,\n" + "             users.id         as userid,\n" + "             users.firstname  as firstName,\n" + "             users.lastname   as lastName,\n" + "             users.image_path as profile_img,\n" + "             p.\"createdAt\"    as created\n" + "      from users\n" + "               join followers f on users.id = f.receiver_id\n" + "               join posts p on users.id = p.user_id\n" + "      where f.receiver_id = " + user_id + "\n" + "      union\n" + "      select p.id             as id,p.title as title,\n" + "             p.description    as description,\n" + "             p.file_path      as post_path,\n" + "             users.id         as userid,\n" + "             users.firstname  as firstName,\n" + "             users.lastname   as lastName,\n" + "             users.image_path as profile_img,\n" + "             p.\"createdAt\"    as created\n" + "      from users\n" + "               join followers f on users.id = f.receiver_id\n" + "               join posts p on users.id = p.user_id\n" + "      where f.sender_id = " + user_id + ") a\n" + "order by a.created desc");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                PostDto post = new PostDto();
                post.setId(resultSet.getInt("id"));
                post.setTitle(resultSet.getString("title"));
                post.setDescription(resultSet.getString("description"));
                post.setUserid(resultSet.getInt("userid"));
                post.setFirstName(resultSet.getString("firstName"));
                post.setLastName(resultSet.getString("lastName"));
                post.setCommentCount(countComments(resultSet.getInt("id")));
                post.setLikesCount(countLikes(resultSet.getInt("id")));
                post.setSaved(bookMark(resultSet.getInt("id"), user_id));


                try {
                    String imgName = "" + resultSet.getString("post_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setPost_path(b64);
                } catch (Exception e) {

                }
                try {
                    String imgName = "" + resultSet.getString("profile_img");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setProfile_img(b64);
                } catch (Exception e) {
                }
                arrayList.add(post);
            }
            con.close();
            return arrayList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int countLikes(Integer post_id) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select count(*)\n" + "from likes\n" + "where post_id=" + post_id + " ;");

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int countComments(Integer post_id) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select count(*)\n" + "from comments\n" + "where post_id=" + post_id + " ;");

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int bookMark(Integer post_id, Integer user_id) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select count(*)\n" + "from saved_post\n" + "where post_id=" + post_id + " and user_id=" + user_id + " ;");

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public PostDto postDetail(int post_id, int user_id) {


        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("    select posts.id as postid,\n" +
                    "       posts.title as title,\n" +
                    "       posts.description as description,\n" +
                    "       posts.file_path as post_path,\n" +
                    "       u.id as userid,\n" +
                    "       u.firstname as firstname,\n" +
                    "       u.lastname as lastname,\n" +
                    "       u.image_path as profile_path,\n" +
                    "       json_agg(cte.*) as comment\n" +
                    "from posts\n" +
                    "join users u on posts.user_id = u.id\n" +
                    "join (\n" +
                    "    select comments.id as commentid,\n" +
                    "           comments.post_id as postid,\n" +
                    "           comments.description as description,\n" +
                    "           u2.id as userid,\n" +
                    "           u2.firstname as firstname,\n" +
                    "           u2.lastname as lastname\n" +
                    "    from comments\n" +
                    "    join users u2 on comments.user_id = u2.id\n" +
                    "    order by comments.\"createdAt\"\n" +
                    "    )cte on cte.postid=posts.id\n" +
                    "where posts.id=" + post_id +
                    "\n" + " group by posts.id, title, posts.description, posts,\n" + "         u.id, u.firstname, u.lastname,u.image_path;");
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                PostDto post = new PostDto();
                post.setId(resultSet.getInt("postid"));
                post.setTitle(resultSet.getString("title"));
                post.setDescription(resultSet.getString("description"));
                post.setUserid(resultSet.getInt("userid"));
                post.setFirstName(resultSet.getString("firstname"));
                post.setLastName(resultSet.getString("lastname"));
                post.setCommentCount(countComments(resultSet.getInt("postid")));
                post.setLikesCount(countLikes(resultSet.getInt("postid")));
                post.setSaved(bookMark(resultSet.getInt("postid"), user_id));

                Array comment = resultSet.getArray("comment");
                String s = comment.toString();
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<CommentDto>>() {
                }.getType();
                post.setComment(gson.fromJson(s, type));
                try {
                    String imgName = "" + resultSet.getString("post_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setPost_path(b64);
                } catch (Exception e) {
                }
                try {
                    String imgName = "" + resultSet.getString("profile_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    post.setProfile_img(b64);
                } catch (Exception e) {
                }
                con.close();
                return post;
            } else {
                return findByIdD(post_id, user_id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
