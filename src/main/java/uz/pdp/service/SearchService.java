package uz.pdp.service;


//Asadbek Xalimjonov 2/14/22 8:39 AM

import uz.pdp.config.ConnectDB;
import uz.pdp.model.User;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SearchService {

    public List<User> collectUser(String search) {

        List<User> userList = new ArrayList<>();
        String data = "";
        if (search.length()>=3) {
            data = search.substring(0,3);
        }else if (search.length()>1){
            data = search.substring(0,1);
        }

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select * from users where users.firstname like '" + data + "%' or lastname like '" + data + "%' or username like '" + data + "%';");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                ser.setUsername(resultSet.getString("username"));

                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                userList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

}
