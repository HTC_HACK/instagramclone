package uz.pdp.service;

//Asadbek Xalimjonov 2/4/22 9:44 PM

import uz.pdp.config.ConnectDB;
import uz.pdp.model.User;
import uz.pdp.repo.CrudRepo;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements CrudRepo<User, String> {

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public boolean save(User user) {
        int status = 0;
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into users(email,password,confirmpassword) values (?,?,?)");
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getConfirmPassword());
            status = ps.executeUpdate();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status > 0;
    }

    @Override
    public boolean edit(User user, String email) {
        try {
            Connection con = ConnectDB.getConnection();
            PreparedStatement ps = con.prepareStatement("update users set username=?,firstname=?,lastname=?,image_name=?,image_path=? where email=?");
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getFirstName());
            ps.setString(3, user.getLastName());
            ps.setString(4, user.getImage_name());
            ps.setString(5, user.getImage_path());
            ps.setString(6, user.getEmail());
            ps.executeUpdate();
            con.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public User findById(String authUser) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select * from users where users.email='" + authUser + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                ser.setUsername(resultSet.getString("username"));
                ser.setPrivateAccount(resultSet.getBoolean("isprivateaccount"));
                ser.setEmail(resultSet.getString("email"));
                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                ser.setPassword(resultSet.getString("password"));
                con.close();
                return ser;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User findByData(User user) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select * from users where users.email='" + user.getEmail() + "' and users.password='" + user.getPassword() + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setEmail(resultSet.getString("email"));
                ser.setPassword(resultSet.getString("password"));
                con.close();
                return ser;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean delete(String id) {
        return false;
    }


    public List<User> getSuggestion(String authUser) {

        List<User> userList = new ArrayList<>();

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select * from users where users.email<>'" + authUser + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                ser.setUsername(resultSet.getString("username"));

                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                userList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<User> getAll(String authUser) {

        List<User> userList = new ArrayList<>();
        User byId = findById(authUser);
        Integer id = byId.getId();

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("with cte as(\n" +
                    "    select\n" +
                    "        users.id         as id,\n" +
                    "        users.firstname  as firstName,\n" +
                    "        users.lastname   as lastName,\n" +
                    "        users.image_path as image_path\n" +
                    "    from users\n" +
                    "             join followers f on users.id = f.receiver_id\n" +
                    "    where f.receiver_id ="+id+"\n" +
                    "    union\n" +
                    "    select\n" +
                    "        users.id         as id,\n" +
                    "        users.firstname  as firstName,\n" +
                    "        users.lastname   as lastName,\n" +
                    "        users.image_path as image_path\n" +
                    "    from users\n" +
                    "             join followers f on users.id = f.receiver_id\n" +
                    "    where f.sender_id ="+id+"\n" +
                    ")\n" +
                    "select *\n" +
                    "from users\n" +
                    "where users.id not in (select cte.id from cte)");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                ser.setUsername(resultSet.getString("username"));

                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                userList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;
    }


    public List<User> getAllFollowers(Integer userId) {

        List<User> userList = new ArrayList<>();

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select users.id,users.firstname,users.lastname,users.image_path\n" + "from users\n" + "         join followers f on users.id = f.sender_id\n" + "where receiver_id=" + userId + " ;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));

                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                userList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;
    }

    public List<User> getAllFollowig(Integer userId) {

        List<User> userList = new ArrayList<>();

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select users.id,users.firstname,users.lastname,users.image_path\n" + "from users\n" + "join followers f on users.id = f.receiver_id\n" + "where sender_id=" + userId + " ;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));

                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                userList.add(ser);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;
    }

    public User findById(String authUser, int user_id) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select * from users where users.id=" + user_id + " ;");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                ser.setUsername(resultSet.getString("username"));
                ser.setPrivateAccount(resultSet.getBoolean("isprivateaccount"));
                ser.setEmail(resultSet.getString("email"));
                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                ser.setPassword(resultSet.getString("password"));
                con.close();
                return ser;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
