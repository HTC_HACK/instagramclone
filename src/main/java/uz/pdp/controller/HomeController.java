package uz.pdp.controller;


//Asadbek Xalimjonov 2/4/22 11:11 AM

import uz.pdp.config.ConnectDB;
import uz.pdp.dto.PostDto;
import uz.pdp.model.User;
import uz.pdp.service.PostServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static uz.pdp.validation.ValidationResult.EMAIL_OR_PASSWORD_INVALID;


@WebServlet("/")
public class HomeController extends HttpServlet {

    UserServiceImpl userService = new UserServiceImpl();
    PostServiceImpl postService = new PostServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            if (authUser != null) {
                User byId = userService.findById(authUser);
                List<User> all = userService.getAll(authUser);
                List<PostDto> homePost = postService.getHomePost(byId.getId());
                req.setAttribute("messageCount",notifyMessage(authUser));
                req.setAttribute("currentUser", byId);
                req.setAttribute("countNotification", notify(authUser));
                req.setAttribute("userList", all);
                req.setAttribute("postList", homePost);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("layouts/body.jsp");
                requestDispatcher.forward(req, resp);
            } else {
//                req.setAttribute("session", SESSION_END.name());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
                requestDispatcher.forward(req, resp);
            }
        } catch (Exception e) {
//            req.setAttribute("session", SESSION_END.name());
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        User user = new User();
        user.setPassword(password);
        user.setEmail(email);

        User byData = userService.findByData(user);


        if (byData != null) {
            HttpSession session = req.getSession();
            session.setAttribute("authUser", byData.getEmail());
            resp.sendRedirect("/");
//            req.setAttribute("login", SUCCESSFULLY_LOGIN.name());
//            RequestDispatcher requestDispatcher = req.getRequestDispatcher("layouts/body.jsp");
//            requestDispatcher.forward(req,resp);
        } else {
            req.setAttribute("error", EMAIL_OR_PASSWORD_INVALID.name());
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }


    }

    public static int notify(String email) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;
        int count = 0;

        try {
            preparedStatement = con.prepareStatement("select count(is_read)\n" + "        from log_notification\n" + "    join users u on u.id = log_notification.receiver_id\n" + "    join users p on p.id = log_notification.sender_id\n" + "    where u.email='" + email + "' and is_read=false;");

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;

    }

    public static int notifyMessage(String auther) {
        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;
        int count = 0;

        try {
            UserServiceImpl userService = new UserServiceImpl();
            Integer user = userService.findById(auther).getId();
            preparedStatement = con.prepareStatement("select count(status)\n" + "        from messages\n" + "  where messages.receiver_id='" + user + "' and status=false;");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

}
