package uz.pdp.controller.post;


//Asadbek Xalimjonov 2/8/22 8:52 AM

import uz.pdp.model.User;
import uz.pdp.service.PostServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/deletePost")
public class DeletePost extends HttpServlet {
    UserServiceImpl userService = new UserServiceImpl();
    PostServiceImpl postService = new PostServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            User ser = userService.findById(authUser);
            try {
                int post_id = Integer.parseInt(req.getParameter("post_id"));
                boolean byId = postService.deletePost(post_id,ser.getId());
                resp.sendRedirect("/profile");
            }catch (Exception e){}
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("post/edit.jsp");
            requestDispatcher.forward(req, resp);
        }

    }
}
