package uz.pdp.controller.post;


//Asadbek Xalimjonov 2/7/22 4:03 PM

import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.model.post.Post;
import uz.pdp.service.PostServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;

@WebServlet("/createPost")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)
public class PostController extends HttpServlet {

    PostServiceImpl postService = new PostServiceImpl();
    UserServiceImpl userService = new UserServiceImpl();
    public static final String UPLOAD_DIR_POST = "postImg";
    public static final String UPLOAD_DIR_VIDEO = "postVideo";
    public String dbFileName = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            User ser = userService.findById(authUser);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("post/create.jsp");
            req.setAttribute("currentUser", ser);
            req.setAttribute("messageCount", HomeController.notifyMessage(authUser));
            req.setAttribute("countNotification", HomeController.notify(authUser));
            requestDispatcher.forward(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("post/create.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            resp.setContentType("text/html;charset=UTF-8");

            int user_id = Integer.parseInt(req.getParameter("user_id"));
            String title = req.getParameter("title");
            String description = req.getParameter("description");

            try {
                Part part = req.getPart("file");//
                String fileName = extractFileName(part);//file name

                String applicationPath = "/home/koh/Desktop/JavaBootCamp/to-do-app/src/main/resources";
                String uploadPath = applicationPath + File.separator + UPLOAD_DIR_POST;
                System.out.println("applicationPath:" + applicationPath);
                File fileUploadDirectory = new File(uploadPath);
                if (!fileUploadDirectory.exists()) {
                    fileUploadDirectory.mkdirs();
                }
                String savePath = uploadPath + File.separator + fileName;
                System.out.println("savePath: " + savePath);
                String sRootPath = new File(savePath).getAbsolutePath();
                System.out.println("sRootPath: " + sRootPath);
                part.write(savePath + File.separator);
                File fileSaveDir1 = new File(savePath);

                dbFileName = UPLOAD_DIR_POST + File.separator + fileName;
                part.write(savePath + File.separator);
                Post post = new Post();
                post.setUser_id(user_id);
                post.setDescription(description);
                post.setTitle(title);
                post.setVideo(false);
                post.setFile_name(dbFileName);
                post.setFile_path(savePath);
                postService.save(post);
                resp.sendRedirect("/profile");
            } catch (Exception e) {
                resp.sendRedirect("/profile");
            }
        }


    }

    private String extractFileName(Part part) {//This method will print the file name.
        String contentDis = part.getHeader("content-disposition");
        String[] items = contentDis.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}
