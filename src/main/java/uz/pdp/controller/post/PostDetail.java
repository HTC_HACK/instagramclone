package uz.pdp.controller.post;


//Asadbek Xalimjonov 2/13/22 12:51 AM

import uz.pdp.controller.HomeController;
import uz.pdp.dto.PostDto;
import uz.pdp.service.PostServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet("/detailPost")
public class PostDetail extends HttpServlet {

    PostServiceImpl postService  = new PostServiceImpl();
    UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            if (authUser != null) {
                int post_id = Integer.parseInt(req.getParameter("post_id"));
                Integer id = userService.findById(authUser).getId();
                PostDto postDto = postService.postDetail(post_id,id);
                req.setAttribute("post",postDto);
                req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
                req.setAttribute("countNotification", HomeController.notify(authUser));
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("post/view.jsp");
                requestDispatcher.forward(req, resp);
            } else {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
                requestDispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
