package uz.pdp.controller.notification;


//Asadbek Xalimjonov 2/11/22 11:21 PM


import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/following")
public class FollowingData extends HttpServlet {
    UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            User byId = userService.findById(authUser);
            Integer user_id = Integer.parseInt(req.getParameter("user_id"));
            req.setAttribute("countNotification", HomeController.notify(authUser));
            req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
            List<User> following = userService.getAllFollowig(user_id);
            req.setAttribute("following", following);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("activity/following.jsp");
            requestDispatcher.forward(req, resp);

        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("activity/following.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
