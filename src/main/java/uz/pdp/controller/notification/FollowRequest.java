package uz.pdp.controller.notification;


//Asadbek Xalimjonov 2/9/22 6:37 PM

import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.service.NotificationServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/followRequest")
public class FollowRequest extends HttpServlet {

    UserServiceImpl userService = new UserServiceImpl();
    NotificationServiceImpl notificationService = new NotificationServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            Integer receiver_id = Integer.parseInt(req.getParameter("follow_id"));
            User ser = userService.findById(authUser);
            req.setAttribute("countNotification", HomeController.notify(authUser));
            req.setAttribute("messageCount", HomeController.notifyMessage(authUser));
            Integer sender_id = ser.getId();
            if (sender_id != receiver_id) {
                notificationService.makeFollow(sender_id, receiver_id);
                resp.sendRedirect("/profile");
            }

        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("activity/notification.jsp");
            requestDispatcher.forward(req, resp);
        }

    }
}
