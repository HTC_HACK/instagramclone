package uz.pdp.controller.notification;


//Asadbek Xalimjonov 2/8/22 5:46 PM

import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.service.NotificationServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet("/activity")
public class NotificationController extends HttpServlet {

    UserServiceImpl userService = new UserServiceImpl();
    NotificationServiceImpl notificationService = new NotificationServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            User ser = userService.findById(authUser);
            List<User> notification = notificationService.getNotification(authUser);
            req.setAttribute("currentUser", ser);
            req.setAttribute("countNotification", HomeController.notify(authUser));
            req.setAttribute("notification",notification);
            req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("activity/notification.jsp");
                requestDispatcher.forward(req, resp);

            for(int i=1;i<5;i++){
                // the thread will sleep for the 500 milli seconds
                try{Thread.sleep(1000);

                }catch(InterruptedException e){System.out.println(e);}
                System.out.println(i);
                if (i==4)
                {
                    notificationService.clearAll(ser.getId());
                }
            }
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("activity/notification.jsp");
            requestDispatcher.forward(req, resp);
        }

    }
}
