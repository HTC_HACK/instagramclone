package uz.pdp.controller.search;


//Asadbek Xalimjonov 2/14/22 8:31 AM

import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.service.SearchService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/searchData")
public class SearchController extends HttpServlet {
    SearchService searchService = new SearchService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            if (authUser != null) {

                String search = req.getParameter("search");
                List<User> userList = searchService.collectUser(search);
                req.setAttribute("result", userList);
                req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
                req.setAttribute("countNotification", HomeController.notify(authUser));
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("activity/search.jsp");
                requestDispatcher.forward(req, resp);

            } else {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
                requestDispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }

    }
}
