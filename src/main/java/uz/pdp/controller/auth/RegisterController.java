package uz.pdp.controller.auth;


//Asadbek Xalimjonov 2/4/22 3:02 PM

import uz.pdp.model.User;
import uz.pdp.service.UserServiceImpl;
import uz.pdp.validation.UserRegistrationValidator;
import uz.pdp.validation.ValidationResult;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static uz.pdp.validation.ValidationResult.*;

@WebServlet("/register")
public class RegisterController extends HttpServlet {

    UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        dispach(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String confirm_password = req.getParameter("confirm_password");

        if (password.equals(confirm_password)) {
            User user = new User(email, password, confirm_password);

            ValidationResult apply = UserRegistrationValidator
                    .isEmailValid()
                    .and(UserRegistrationValidator.isPasswordValid())
                    .apply(user);

            if (apply.equals(SUCCESS)) {
                boolean save = userService.save(user);
                if(save)
                {
                    req.setAttribute("successregister",SUCCESSFULLY_REGISTERED.name());
                    RequestDispatcher dispatcher = req.getRequestDispatcher("auth/registered.jsp");
                    dispatcher.forward(req, resp);
                }else{
                    req.setAttribute("error",ERROR.name());
                    dispach(req, resp);
                }
            } else {
                String result = apply.name();
                req.setAttribute("result", result);
                dispach(req, resp);
            }

        } else {
            req.setAttribute("match_error", PASSWORD_DID_NOT_MATCH);
            dispach(req, resp);
        }


    }

    private void dispach(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("auth/register.jsp");
        dispatcher.forward(req, resp);
    }

}
