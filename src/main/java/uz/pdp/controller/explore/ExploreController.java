package uz.pdp.controller.explore;



//Asadbek Xalimjonov 2/14/22 3:02 PM

import uz.pdp.controller.HomeController;
import uz.pdp.dto.UserDto;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/explore")
public class ExploreController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            RequestDispatcher requestDispatcher;
            if (authUser != null) {
                requestDispatcher = req.getRequestDispatcher("explore/explore.jsp");
            } else {
                requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            }
            requestDispatcher.forward(req, resp);
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }

    }
}
