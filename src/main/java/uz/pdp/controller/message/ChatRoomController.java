package uz.pdp.controller.message;


//Asadbek Xalimjonov 2/14/22 10:28 AM

import uz.pdp.config.ConnectDB;
import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.model.message.Message;
import uz.pdp.service.MessageService;
import uz.pdp.service.UserServiceImpl;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/chatRoom")
public class ChatRoomController extends HttpServlet {
    MessageService messageService = new MessageService();
    UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            RequestDispatcher requestDispatcher;
            if (authUser != null) {
                Integer sender = userService.findById(authUser).getId();
                int receiver = Integer.parseInt(req.getParameter("user_id"));
                String description = req.getParameter("description");
                messageService.saveMessage(sender,receiver,description);
                String redirect_url = req.getParameter("redirect_url");
                redirect_url += receiver;
                resp.sendRedirect(redirect_url);
            } else {
                requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
                requestDispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            RequestDispatcher requestDispatcher;
            if (authUser != null) {
                Integer you = userService.findById(authUser).getId();
                int friend = Integer.parseInt(req.getParameter("user_id"));
                User friend1 = findFriend(friend);
                List<Message> messageDtoList = messageService.getChatRoomMessage(you, friend);
                req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
                req.setAttribute("friend", friend1);
                req.setAttribute("messages", messageDtoList);
                req.setAttribute("countNotification", HomeController.notify(authUser));
                requestDispatcher = req.getRequestDispatcher("message/chatroom.jsp");
                for(int i=1;i<5;i++){
                    // the thread will sleep for the 500 milli seconds
                    try{Thread.sleep(1000);

                    }catch(InterruptedException e){System.out.println(e);}
                    System.out.println(i);
                    if (i==4)
                    {
                        messageService.readAll(friend,you);
                    }
                }
            } else {
                requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            }
            requestDispatcher.forward(req, resp);
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    public User findFriend(Integer authUser) {

        Connection con = ConnectDB.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = con.prepareStatement("select * from users where users.id=" + authUser + " ;");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User ser = new User();
                ser.setId(resultSet.getInt("id"));
                ser.setFirstName(resultSet.getString("firstname"));
                ser.setLastName(resultSet.getString("lastname"));
                try {
                    String imgName = "" + resultSet.getString("image_path");
                    BufferedImage bImage = ImageIO.read(new File(imgName));//give the path of an image
                    ByteArrayOutputStream base = new ByteArrayOutputStream();
                    ImageIO.write(bImage, "png", base);
                    base.flush();
                    byte[] imageInByteArray = base.toByteArray();
                    base.close();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    ser.setImage_path(b64);
                } catch (Exception e) {
                }
                con.close();
                return ser;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
