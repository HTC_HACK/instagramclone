package uz.pdp.controller.message;


//Asadbek Xalimjonov 2/14/22 9:08 AM

import uz.pdp.controller.HomeController;
import uz.pdp.dto.UserDto;
import uz.pdp.model.User;
import uz.pdp.service.MessageService;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/sendMessage")
public class MessageController extends HttpServlet {
    MessageService messageService = new MessageService();
    UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            RequestDispatcher requestDispatcher;
            if (authUser != null) {
                Integer user_id = userService.findById(authUser).getId();
                List<UserDto> allFriends = messageService.getAllFriends(user_id);
                req.setAttribute("allFriends", allFriends);
                req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
                req.setAttribute("countNotification", HomeController.notify(authUser));
                requestDispatcher = req.getRequestDispatcher("message/message.jsp");
            } else {
                requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            }
            requestDispatcher.forward(req, resp);
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
