package uz.pdp.controller.profile;


//Asadbek Xalimjonov 2/11/22 11:34 PM

import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.model.post.Post;
import uz.pdp.service.NotificationServiceImpl;
import uz.pdp.service.PostServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/showProfile")
public class ShowProfileController extends HttpServlet {

    NotificationServiceImpl notificationService = new NotificationServiceImpl();
    UserServiceImpl userService = new UserServiceImpl();
    PostServiceImpl postService = new PostServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            User byId = userService.findById(authUser);
            int user_id = Integer.parseInt(req.getParameter("user_id"));
            User ser = userService.findById(authUser, user_id);
            if (byId.getId() != user_id && ser != null) {
                //write image
                List<Post> allById = postService.getAllById(user_id);
                int following = notificationService.following(user_id);
                int followers = notificationService.followers(user_id);

                int num = notificationService.followersFind(user_id, byId.getId());

                req.setAttribute("num", num);
                req.setAttribute("countNotification", HomeController.notify(authUser));
                req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
                req.setAttribute("currentUser", ser);
                req.setAttribute("count", allById.size());
                req.setAttribute("postList", allById);
                req.setAttribute("following", following);
                req.setAttribute("followers", followers);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("profile/showProfile.jsp");
                requestDispatcher.forward(req, resp);
            } else {
                resp.sendRedirect("/profile");
            }
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("profile/profile.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
