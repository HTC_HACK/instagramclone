package uz.pdp.controller.profile;

import uz.pdp.controller.HomeController;
import uz.pdp.model.User;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;

import static uz.pdp.validation.ValidationResult.SUCCESSFULLY_UPDATED;
import static uz.pdp.validation.ValidationResult.UPDATING_ERROR;


//Asadbek Xalimjonov 2/6/22 5:56 PM

@WebServlet("/editProfile")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)

public class EditProfileController extends HttpServlet {

    UserServiceImpl userService = new UserServiceImpl();
    public static final String UPLOAD_DIR = "profileImg";
    public String dbFileName = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        String email = req.getParameter("email");
        if (authUser != null) {
            User ser = userService.findById(email);
            req.setAttribute("countNotification", HomeController.notify(authUser));
            req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
            req.setAttribute("currentUser", ser);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("profile/edit.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("profile/edit.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            resp.setContentType("text/html;charset=UTF-8");

            String username = req.getParameter("username");
            String firstName = req.getParameter("firstName");
            String lastName = req.getParameter("lastName");
            String email = req.getParameter("email");


            Part part = req.getPart("file");//
            String fileName = extractFileName(part);//file name

            String applicationPath = "/home/koh/Desktop/JavaBootCamp/to-do-app/src/main/resources";
//        String applicationPath = getServletContext().getRealPath("");
            String uploadPath = applicationPath + File.separator + UPLOAD_DIR;
            System.out.println("applicationPath:" + applicationPath);
            File fileUploadDirectory = new File(uploadPath);
            if (!fileUploadDirectory.exists()) {
                fileUploadDirectory.mkdirs();
            }
            String savePath = uploadPath + File.separator + fileName;
            System.out.println("savePath: " + savePath);
            String sRootPath = new File(savePath).getAbsolutePath();
            System.out.println("sRootPath: " + sRootPath);
            part.write(savePath + File.separator);
            File fileSaveDir1 = new File(savePath);

            dbFileName = UPLOAD_DIR + File.separator + fileName;
            part.write(savePath + File.separator);


            User user = new User();
            user.setEmail(email);
            user.setUsername(username);
            user.setLastName(lastName);
            user.setFirstName(firstName);
            user.setImage_path(savePath);
            user.setImage_name(dbFileName);

            boolean edit = userService.edit(user, email);
            if (edit) {
                session.setAttribute("updated", SUCCESSFULLY_UPDATED.name());
            } else {
                session.setAttribute("update_error", UPDATING_ERROR.name());
            }
            resp.sendRedirect("/profile");
        }
    }


    private String extractFileName(Part part) {//This method will print the file name.
        String contentDis = part.getHeader("content-disposition");
        String[] items = contentDis.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}
