package uz.pdp.controller.profile;


//Asadbek Xalimjonov 2/5/22 7:11 PM


import uz.pdp.controller.HomeController;
import uz.pdp.dto.PostDto;
import uz.pdp.model.User;
import uz.pdp.model.post.Post;
import uz.pdp.service.NotificationServiceImpl;
import uz.pdp.service.PostServiceImpl;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/profile")
public class ProfileController extends HttpServlet {

    UserServiceImpl userService = new UserServiceImpl();
    PostServiceImpl postService = new PostServiceImpl();
    NotificationServiceImpl notificationService = new NotificationServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String authUser = (String) session.getAttribute("authUser");
        if (authUser != null) {
            User ser = userService.findById(authUser);

            //write image
            List<Post> allById = postService.getAllById(ser.getId());
            int following = notificationService.following(ser.getId());
            int followers = notificationService.followers(ser.getId());
            List<PostDto> savedPost = postService.getSavedPost(ser.getId());

            req.setAttribute("currentUser", ser);
            req.setAttribute("savedPost",savedPost);
            req.setAttribute("count", allById.size());
            req.setAttribute("postList", allById);
            req.setAttribute("following", following);
            req.setAttribute("followers", followers);
            req.setAttribute("messageCount",HomeController.notifyMessage(authUser));
            req.setAttribute("countNotification", HomeController.notify(authUser));
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("profile/profile.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("profile/profile.jsp");
            requestDispatcher.forward(req, resp);
        }


    }
}
