package uz.pdp.controller.comment;


//Asadbek Xalimjonov 2/13/22 2:21 PM

import uz.pdp.model.User;
import uz.pdp.service.LikeService;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/like")
public class LikeController extends HttpServlet {
    UserServiceImpl userService = new UserServiceImpl();
    LikeService likeService = new LikeService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            if (authUser != null) {
                User byId = userService.findById(authUser);
                int user_id = byId.getId();
                Integer post_id = Integer.parseInt(req.getParameter("post_id"));
                likeService.saveLike(user_id,post_id);
                resp.sendRedirect("/");
            } else {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
                requestDispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }


    }
}
