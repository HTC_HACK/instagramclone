package uz.pdp.controller.comment;


//Asadbek Xalimjonov 2/12/22 10:34 PM

import uz.pdp.model.User;
import uz.pdp.model.post.Comment;
import uz.pdp.service.CommentService;
import uz.pdp.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/comment")
public class CommentController extends HttpServlet {

    UserServiceImpl userService = new UserServiceImpl();
    CommentService commentService = new CommentService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session1 = req.getSession();
            String authUser = (String) session1.getAttribute("authUser");
            if (authUser != null) {
                User byId = userService.findById(authUser);
                int user_id = byId.getId();
                Integer post_id = Integer.parseInt(req.getParameter("post_id"));
                String description = req.getParameter("description");
                String redirect_url = req.getParameter("redirect_url");
                Comment comment = new Comment();
                comment.setPost_id(post_id);
                comment.setDescription(description);
                comment.setUser_id(user_id);
                commentService.saveComment(comment);
                resp.sendRedirect(redirect_url);
            } else {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
                requestDispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("auth/login.jsp");
            requestDispatcher.forward(req, resp);
        }


    }
}
