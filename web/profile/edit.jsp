<%--
  Created by IntelliJ IDEA.
  User: koh
  Date: 2/6/22
  Time: 5:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${currentUser.username}</title>
    <%@ include file="/layouts/link.html" %>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="/layouts/menu.jsp" %>

<div class="container" style="padding: 3% 10%;">
    <a href="/profile" class="btn btn-warning"><i class="fas fa-arrow-left"></i> Back</a>
    <div class="row">
        <div class="col-md-9" style="padding-top:2rem">
            <form action="editProfile" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="hidden" name="email" value="<%=email%>">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" value="${currentUser.username}" required>
                </div>
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="firstName" class="form-control" value="${currentUser.firstName}" required>
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastName" class="form-control" value="${currentUser.lastName}" required>
                </div>
                <div class="form-group">
                    <label>Upload image</label>
                        <input style="background: #efefef;border:none" type="file" name="file" class="form-control" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info"><i class="fas fa-edit"> Update</i> </button>
                </div>
            </form>
        </div>
    </div>
</div>

<%@ include file="/layouts/scripts.html" %>
</body>
</html>
