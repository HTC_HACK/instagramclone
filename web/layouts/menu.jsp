<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand-lg navbar-light" style="background: #fff">
    <div class="container" style="padding-right: 7%;padding-left: 7%">
        <a class="navbar-brand d-flex align-items-center" href="/">
            <h6 style="font-size: 20px;font-family: sans-serif;font-weight: 400;padding-top: 0.5rem">Instagram</h6>
        </a>
        <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse"
                data-target="#navbar4">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar4">

            <ul class="navbar-nav mr-auto pl-lg-4" style="float: right">
                <form class="form-inline my-2 my-lg-0" action="/searchData" method="post">
                    <input name="search" class="form-control mr-sm-2" type="text" placeholder="Search">
                    <input type="submit" value="Search" class="btn btn-success">
                </form>
            </ul>
            <ul class="navbar-nav ml-auto mt-6 mt-lg-0">
                <li class="nav-item"><a class="nav-link" href="/">
                    <i class="fas fa-home icon" title="Home"></i>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="/sendMessage">
                    <i class="fab fa-facebook-messenger icon" title="Message"></i><span class="badge badge-danger">${messageCount}</span>
                    <span class="sr-only">unread messages</span>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="/createPost">
                    <i class="fas fa-plus-circle icon" title="Create Post"></i>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="/explore">
                    <i class="fas fa-compass icon" title="Explore"></i>
                </a></li>
                <li class="nav-item"><a class="nav-link" href="/activity">
                    <i class="fas fa-heart icon" title="Activity"></i><span class="badge badge-danger">${countNotification}</span>
                    <span class="sr-only">unread messages</span>
                </a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user icon"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/profile"><i class="fas fa-user"></i> Profile</a>
                        <a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt icon"></i> Log out</a>
                    </div>
                </li>
            </ul>
        </div>

    </div>
</nav>
