<%@ page import="static uz.pdp.validation.ValidationResult.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <%@ include file="link.html" %>
    <style>
        .modal-lg {
            max-width: 200% !important;
        }
    </style>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="menu.jsp" %>
<div class="container" style="padding: 3% 7%;">

    <div class="row">
        <div class="col-md-8">
            <!-- Button trigger modal -->


            <c:forEach items="${postList}" var="post">
                <div class="card" style="width: 100%;margin-top: 2%">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a style="text-decoration: none;color: #000"
                                                       href="/showProfile?user_id=${post.userid}"> <c:if
                                test="${post.profile_img!=null}">
                            <img
                                    src="data:image/png;base64, ${post.profile_img}"
                                    width="30px" height="30px" style="border-radius: 50%">
                        </c:if> ${post.firstName} ${post.lastName}</a>
                        </li>
                    </ul>
                    <a href="/detailPost?post_id=${post.id}"> <img class="card-img-top"
                                                                   src="data:image/png;base64,${post.post_path}"
                                                                   alt="Card image cap" style="height: 330px"></a>
                    <div class="card-body">

                        <a style="text-decoration: none;color: black"
                           href="/detailPost?post_id=${post.id}"> ${post.commentCount} <i
                                class="fas fa-comment"></i></a>
                        <a style="text-decoration: none;color: black"
                           href="/like?post_id=${post.id}"> ${post.likesCount} <i class="fa fa-thumbs-up icon"></i></a>
                        <c:if test="${post.saved>0}">
                            <a style="text-decoration: none;color: black;float: right" href="/saved?post_id=${post.id}">Saved
                                <i class="fas fa-bookmark"></i> </a>
                        </c:if>
                        <c:if test="${post.saved<1}">
                            <a style="text-decoration: none;color: black;float: right" href="/saved?post_id=${post.id}">UnSaved
                                <i class="fas fa-bookmark"></i> </a>
                        </c:if>

                        <p class="card-text">${post.title} ${post.description}</p>
                    </div>

                    <div class="card-body">
                        <form method="post" action="/comment" class="form-inline my-2 my-lg-0">
                            <input name="post_id" type="hidden" value="${post.id}">
                            <input type="hidden" name="redirect_url" value="/">
                            <input name="description" class="form-control mr-sm-2" type="search"
                                   placeholder="Add comment"
                                   aria-label="Search"
                                   style="width: 90%">
                            <button class="btn btn-outline-info my-2 my-sm-0" type="submit"><i
                                    class="fas fa-paper-plane send"></i></button>
                        </form>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="col-md-4">
            <div class="you">
                <a href="/profile" style="text-decoration: none;color: #000">
                    <c:if test="${currentUser.image_path!=null}">
                        <img
                                src="data:image/png;base64, ${currentUser.image_path}"
                                width="30px" height="30px" style="border-radius: 50%">
                    </c:if>
                    ${currentUser.firstName} ${currentUser.lastName}</a>
            </div>
            <hr>
            <div class="suggestion">
                <h6>Suggestion for you</h6>
                <c:forEach items="${userList}" var="user">
                    <c:if test="${currentUser.id!=user.id}">
                        <div style="padding-top:1rem ">
                            <a href="/showProfile?user_id=${user.id}" style="text-decoration: none;color: #000">
                                <c:if test="${user.image_path!=null}">
                                    <img
                                            src="data:image/png;base64, ${user.image_path}"
                                            width="30px" height="30px" style="border-radius: 50%">
                                </c:if>
                                    ${user.firstName} ${user.lastName}</a>
                            <a href="/followRequest?follow_id=${user.id}" class="btn btn-info">follow</a>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<%@ include file="scripts.html" %>
</body>
</html>
