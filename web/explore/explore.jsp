<%@ page import="static uz.pdp.validation.ValidationResult.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Explore</title>
    <%@ include file="../layouts/link.html" %>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="../layouts/menu.jsp" %>
<div class="container" style="padding: 3% 7%;">
    <div class="row">

        <div class="col-md-4">
            <div class="card" style="width:100%;" >
                <img style="height: 280px;width: 325px" src="https://cdn.cbeditz.com/cbeditz/preview/insta-model-picsart-cb-editing-hd-background-11626892035xclxrsvc6j.jpg" class="card-img-top" alt="...">
            </div>
        </div>

        <div class="col-md-4">
            <div class="card" style="width:100%">
                <img style="height: 280px;width: 325px" src="https://cdn.cbeditz.com/cbeditz/preview/insta-model-picsart-cb-editing-hd-background-11626892035xclxrsvc6j.jpg" class="card-img-top" alt="...">
            </div>
        </div>

        <div class="col-md-4">
            <div class="card" style="width:100%">
                <img style="height: 280px;width: 325px" src="https://cdn.cbeditz.com/cbeditz/preview/insta-model-picsart-cb-editing-hd-background-11626892035xclxrsvc6j.jpg" class="card-img-top" alt="...">
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="width:100%">
                <img style="height: 280px;width: 325px" src="https://cdn.cbeditz.com/cbeditz/preview/insta-model-picsart-cb-editing-hd-background-11626892035xclxrsvc6j.jpg" class="card-img-top" alt="...">
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="width:100%">
                <img style="height: 280px;width: 325px" src="https://cdn.cbeditz.com/cbeditz/preview/insta-model-picsart-cb-editing-hd-background-11626892035xclxrsvc6j.jpg" class="card-img-top" alt="...">
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="width:100%">
                <img style="height: 280px;width: 325px" src="https://cdn.cbeditz.com/cbeditz/preview/insta-model-picsart-cb-editing-hd-background-11626892035xclxrsvc6j.jpg" class="card-img-top" alt="...">
            </div>
        </div>


    </div>

</div>
<%@ include file="../layouts/scripts.html" %>
</body>
</html>
