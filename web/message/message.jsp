<%@ page import="static uz.pdp.validation.ValidationResult.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Message</title>
    <%@ include file="../layouts/link.html" %>
    <style>
        .modal-lg {
            max-width: 200% !important;
        }
    </style>
</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="../layouts/menu.jsp" %>

<div class="container" style="padding: 3% 10%;">
    <div class="row">
        <div class="col-md-5" style="overflow-y: scroll;max-height: 76vh">
            <div class="card" style="width: 100%">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Friends</li>
                    <c:forEach items="${allFriends}" var="user">
                        <li class="list-group-item">
                            <a href="/chatRoom?user_id=${user.id}" style="text-decoration: none;">
                                <c:if test="${user.image_path!=null}">
                                    <img
                                            src="data:image/png;base64, ${user.image_path}"
                                            width="30px" height="30px" style="border-radius: 50%">
                                </c:if>
                                    ${user.firstName} ${user.lastName}</a><span style="float: right"
                                                                                class="badge badge-danger">${user.amount}</span>
                            <span class="sr-only">unread messages</span>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="col-md-7" style="background: white;height: 76vh">
            <div style="font-size: 20px;padding: 20% 20%;text-align: center">select chat to start message</div>
        </div>
    </div>
</div>

<%@ include file="../layouts/scripts.html" %>
</body>
</html>
