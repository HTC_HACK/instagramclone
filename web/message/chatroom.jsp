<%@ page import="static uz.pdp.validation.ValidationResult.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Message</title>
    <%@ include file="../layouts/link.html" %>

</head>
<body>
<%
    String email = (String) session.getAttribute("authUser");
    if (email != null) {
        session.setAttribute("authUser", email);
    } else {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("../auth/login.jsp");
        requestDispatcher.forward(request, response);
    }

%>
<%@ include file="../layouts/menu.jsp" %>

<div class="container" style="padding: 3% 10%;">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group">

                <li class="list-group-item">
                    <c:if test="${friend.image_path!=null}">
                    <a style="text-decoration: none;color: #000"
                       href="/showProfile?user_id=${friend.id}"><img
                            src="data:image/png;base64, ${friend.image_path}"
                            width="30px" height="30px" style="border-radius: 50%">
                        </c:if>
                        ${friend.firstName} ${friend.lastName}</a>

                <a href="/sendMessage"
                   style="float: right"
                   class="btn btn-danger"><i
                        class="fas fa-arrow-left"></i> </a></li>
            </ul>
            <ul class="list-group" style="height: 60vh;display: flex;
  flex-direction: column-reverse;overflow-y: scroll;
            background: linear-gradient(rgba(0,0,150,0.5),rgba(0,150,0,0.5),rgba(0,150,0,0.5))">

                <c:forEach items="${messages}" var="message">
                    <c:if test="${message.sender_id==friend.id}">
                        <hr>
                        <div style="float: left;padding-left: 0.5rem">
                            <li class="list-group-item" style="width: 50%">Date : ${message.created_at}
                                <br> ${message.description}</li>
                        </div>
                    </c:if>
                    <c:if test="${message.sender_id!=friend.id}">
                        <hr>
                        <div style="text-align: right;float: right;margin-left: 50%;padding-right: 0.5rem">
                            <li class="list-group-item">Date ${message.created_at} <br> ${message.description}</li>
                        </div>
                    </c:if>
                </c:forEach>
                <hr>


            </ul>
            <form method="post" action="/chatRoom" style="background: white">
                <input name="user_id" value="${friend.id}" type="hidden">
                <input type="hidden" name="redirect_url" value="chatRoom?user_id=">
                <input name="description" class="form-control" type="search" placeholder="Send Message"
                       style="width: 92%">
                <div style="float: right;margin-top: -2.3rem">
                    <button class="btn btn-outline-info my-2 my-sm-0" type="submit"><i
                            class="fas fa-paper-plane send"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>

<%@ include file="../layouts/scripts.html" %>
</body>
</html>
